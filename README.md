# payload

This repository contains schematics and footprints of our new payload interface standard for CubeSats. We use a distributed philosophy in which every payload has a microcontroller, therefore, the PC/104 interface had more pins than the ones we needed, and the separation between PCBs was too rigid. Also, in some 3U structures there is a mechanical conflict in between units. Our solution is to use Molex Pico-Lock connectors for energy and communication between boards. The footprints have also a suggested PCB boundary that facilitates the cable management of the CubeSat.

![](img/KiCAD_schematic.png)
![](img/KiCAD_3D_view_1.png)
![](img/KiCAD_3D_view_2.png)
